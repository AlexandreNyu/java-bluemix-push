/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bnpparibas.bluemix.push;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Alexandre Mommers
 */
@Configuration
@ComponentScan("com.bnpparibas.bluemix.push")
public class BluemixPushServiceTestConfiguration {

    @Bean
    public BluemixPushConfiguration bluemixPushConfiguration() {
        return new BluemixPushConfiguration();
    }
}

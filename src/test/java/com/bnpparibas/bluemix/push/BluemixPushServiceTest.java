/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bnpparibas.bluemix.push;

import com.bnpparibas.bluemix.push.bean.BluemixPushEnvironment;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Alexandre Mommers
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {BluemixPushServiceTestConfiguration.class})
public class BluemixPushServiceTest {

    @Autowired
    private BluemixPushService instance;

    public BluemixPushServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setCurrentEnvironment method, of class BluemixPushService.
     */
    @Test
    public void testSetCurrentEnvironment() {
        System.out.println("setCurrentEnvironment");
        instance.setCurrentEnvironment(BluemixPushEnvironment.production);
        Assert.assertEquals(BluemixPushEnvironment.production, instance.getCurrentEnvironment());
        instance.setCurrentEnvironment(BluemixPushEnvironment.sandbox);
        Assert.assertEquals(BluemixPushEnvironment.sandbox, instance.getCurrentEnvironment());

    }

    /**
     * Test of getCurrentEnvironment method, of class BluemixPushService.
     */
    @Test
    public void testGetCurrentEnvironment() {
        System.out.println("getCurrentEnvironment");
        Assert.assertNotNull(instance.getCurrentEnvironment());
    }

    /**
     * Test of addTag method, of class BluemixPushService.
     */
    @Test
    public void testAddTag() {
        System.out.println("addTag");
        String name = "test-tag";
        String description = "tag de test";
        instance.addTag(name, description);
    }

}

package com.bnpparibas.bluemix.push.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author alexandre mommers
 */
@Getter
@Setter
@ToString
public class TagInfoResponse {

    private String description;
    private String name;
    private String createdTime;
    private String lastUpdatedTime;
    private String createdMode;
}

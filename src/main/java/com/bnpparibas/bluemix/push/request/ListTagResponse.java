package com.bnpparibas.bluemix.push.request;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author alexandre mommers
 */
@Getter
@Setter
@ToString
public class ListTagResponse {

    private List<Tag> tags;

    @Getter
    @Setter
    @ToString
    static public class Tag {

        private String uri;
        private String name;
        private String description;
        private String createdTime;
        private String lastUpdatedTime;
        private String createdMode;
        private String href;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bnpparibas.bluemix.push.bean;

/**
 *
 * @author Alexandre Mommers
 */
public class BluemixPushGetEnvironmentResponse {

    private BluemixPushEnvironment environment;
    private String settings;
    private String id;

    public BluemixPushEnvironment getEnvironment() {
        return environment;
    }

    public void setEnvironment(BluemixPushEnvironment environment) {
        this.environment = environment;
    }

    public String getSettings() {
        return settings;
    }

    public void setSettings(String settings) {
        this.settings = settings;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}

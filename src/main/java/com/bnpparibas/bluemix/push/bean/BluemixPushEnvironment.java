/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bnpparibas.bluemix.push.bean;

/**
 *
 * @author Alexandre Mommers
 */
public enum BluemixPushEnvironment {

    sandbox("sandbox"),
    production("production");

    private final String name;

    BluemixPushEnvironment(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}

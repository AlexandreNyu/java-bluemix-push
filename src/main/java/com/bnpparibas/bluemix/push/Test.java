package com.bnpparibas.bluemix.push;

import com.bnpparibas.bluemix.push.request.TagInfoResponse;

/**
 *
 * @author alexandremommers
 */
public class Test {

    public static void main(String[] args) {
        BluemixPushService testService = new BluemixPushService();

        //testService.deleteTag("test");
        //testService.addTag("test", "na");
        //testService.updateTag("test", "na 3");
        //ListTagResponse response = testService.getTagList();
        //System.out.println(response);
        TagInfoResponse tagInfo = testService.getTagInfo("test 2");
        System.out.println(tagInfo);
    }

}

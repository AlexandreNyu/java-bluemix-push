/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bnpparibas.bluemix.push;

/**
 *
 * @author Alexandre Mommers
 */
public class BluemixPushConfiguration {

    private String baseUrl = "https://mobile.eu-gb.bluemix.net/";
    private String applicationIdentifier = "a31fed3d-cae4-43a2-beee-6cf1c4e101b9";
    private String applicationSecret = "c104ef02-1a80-417c-a984-6bdaa989d85c";

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getApplicationIdentifier() {
        return applicationIdentifier;
    }

    public void setApplicationIdentifier(String applicationIdentifier) {
        this.applicationIdentifier = applicationIdentifier;
    }

    public String getApplicationSecret() {
        return applicationSecret;
    }

    public void setApplicationSecret(String applicationSecret) {
        this.applicationSecret = applicationSecret;
    }

}

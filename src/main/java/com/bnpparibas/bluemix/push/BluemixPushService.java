/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bnpparibas.bluemix.push;

import com.bnpparibas.bluemix.push.bean.BluemixPushEnvironment;
import com.bnpparibas.bluemix.push.bean.BluemixPushGetEnvironmentResponse;
import com.bnpparibas.bluemix.push.exception.AuthentificationException;
import com.bnpparibas.bluemix.push.exception.TagAlreadyExistsException;
import com.bnpparibas.bluemix.push.exception.TagNotFoundException;
import com.bnpparibas.bluemix.push.request.AddTagRequest;
import com.bnpparibas.bluemix.push.request.ListTagResponse;
import com.bnpparibas.bluemix.push.request.TagInfoResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Alexandre Mommers
 */
@Service
public class BluemixPushService {

    @Autowired(required = false)
    private BluemixPushConfiguration bluemixPushConfiguration = new BluemixPushConfiguration();
    private final RestTemplate restTemplate = new RestTemplate();

    /**
     * logger
     */
    protected final Logger LOG = LoggerFactory.getLogger(this.getClass());

    // <editor-fold desc="Environment">
    public void setCurrentEnvironment(BluemixPushEnvironment environment) {
        String url = getBaseUrl();

        HttpEntity<String> entity = getHttpEntity("{\"environment\":\"" + environment.getName() + "\"}");

        /* ignore result if returning a code 200 */
        restTemplate.exchange(url, HttpMethod.PUT, entity, String.class);
    }

    public BluemixPushEnvironment getCurrentEnvironment() {
        String url = getBaseUrl();

        return restTemplate.exchange(url, HttpMethod.GET, getHttpEntity(""), BluemixPushGetEnvironmentResponse.class)
                .getBody()
                .getEnvironment();
    }
    // </editor-fold>

    // <editor-fold desc="Device">
    // </editor-fold>
    // <editor-fold desc="Message">
    // </editor-fold>
    // <editor-fold desc="Tag">
    public void addTag(String name, String description) throws RestClientException {
        AddTagRequest request = new AddTagRequest();
        request.setName(name);
        request.setDescription(description);

        String url = getTagBaseUrl();

        HttpEntity<String> entity = getHttpEntityWithObject(request);

        /* ignore result if returning a code 200 */
        try {
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
            LOG.debug("response with status {} with body {}", response.getStatusCode(), response.getBody());

        } catch (HttpClientErrorException ex) {

            if (HttpStatus.CONFLICT == ex.getStatusCode()) {
                throw new TagAlreadyExistsException(name, ex);
            } else if (HttpStatus.UNAUTHORIZED == ex.getStatusCode()) {
                throw new AuthentificationException(ex);
            }

            throw ex;
        }

    }

    public void updateTag(String name, String description) {
        AddTagRequest request = new AddTagRequest();
        request.setName(name);
        request.setDescription(description);

        String url = getTagBaseUrl() + "/" + name;

        HttpEntity<String> entity = getHttpEntityWithObject(request);

        /* ignore result if returning a code 200 */
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class);
        LOG.debug("response with status {} with body {}", response.getStatusCode(), response.getBody());

    }

    public void deleteTag(String name) {

        String url = getTagBaseUrl() + "/" + name;

        HttpEntity<String> entity = getHttpEntity(null);

        /* ignore result if returning a code 200 */
        try {
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.DELETE, entity, String.class);
            LOG.debug("response with status {} with body {}", response.getStatusCode(), response.getBody());

        } catch (HttpClientErrorException ex) {

            if (HttpStatus.NOT_FOUND == ex.getStatusCode()) {
                throw new TagNotFoundException(name, ex);
            } else if (HttpStatus.UNAUTHORIZED == ex.getStatusCode()) {
                throw new AuthentificationException(ex);
            }

            throw ex;
        }
    }

    public ListTagResponse getTagList() {

        String url = getTagBaseUrl();

        HttpEntity<String> entity = getHttpEntity(null);

        /* ignore result if returning a code 200 */
        ResponseEntity<ListTagResponse> response = restTemplate.exchange(url, HttpMethod.GET, entity, ListTagResponse.class);
        return response.getBody();

    }

    public TagInfoResponse getTagInfo(String name) {

        String url = getTagBaseUrl() + "/" + name;

        HttpEntity<String> entity = getHttpEntity(null);
        try {
            /* ignore result if returning a code 200 */
            HttpEntity<TagInfoResponse> response = restTemplate.exchange(url, HttpMethod.GET, entity, TagInfoResponse.class);
            return response.getBody();
        } catch (HttpClientErrorException ex) {

            if (HttpStatus.NOT_FOUND == ex.getStatusCode()) {
                throw new TagNotFoundException(name, ex);
            }

            throw ex;
        }
    }
    // </editor-fold>

    /*
     public void subscribeDeviceToTag() {

     }

     public void unsubscribeDeviceToTag() {

     }

     public void countDeviceForTag() {

     }

     public void getDeviceListForTag() {

     }*/
    /*public void sendPush() {

     }*/
    // <editor-fold desc="Utils">
    private String getBaseUrl() {
        return bluemixPushConfiguration.getBaseUrl() + "imfpush/v1/apps/" + bluemixPushConfiguration.getApplicationIdentifier();
    }

    private String getTagBaseUrl() {
        return getBaseUrl() + "/tags";
    }

    private HttpEntity getHttpEntity(String json) {

        LOG.debug("request will be created with json : {}", json);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("appSecret", bluemixPushConfiguration.getApplicationSecret());

        HttpEntity<String> entity = new HttpEntity<>(json, headers);

        return entity;
    }

    private HttpEntity<String> getHttpEntityWithObject(Object object) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return getHttpEntity(mapper.writeValueAsString(object));
        } catch (JsonProcessingException ex) {
            LOG.error("fail to convert object to JSON", ex);
            return getHttpEntity("");
        }
    }
    // </editor-fold>
}

package com.bnpparibas.bluemix.push.exception;

import org.springframework.web.client.RestClientException;

/**
 *
 * @author alexandre mommers
 */
public class TagAlreadyExistsException extends RestClientException {

    public TagAlreadyExistsException(String tagName, Throwable cause) {
        super("Tag allready exists with name " + tagName, cause);
    }
}

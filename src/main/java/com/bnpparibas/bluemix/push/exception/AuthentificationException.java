package com.bnpparibas.bluemix.push.exception;

import org.springframework.web.client.RestClientException;

/**
 *
 * @author alexandre mommers
 */
public class AuthentificationException extends RestClientException {

    public AuthentificationException(Throwable ex) {
        super("The application secret is incorrect", ex);
    }

}

package com.bnpparibas.bluemix.push.exception;

import org.springframework.web.client.RestClientException;

/**
 *
 * @author alexandre mommers
 */
public class TagNotFoundException extends RestClientException {

    public TagNotFoundException(String tagName, Throwable cause) {
        super("Tag not found with name " + tagName, cause);
    }
}
